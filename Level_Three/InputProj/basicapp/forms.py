from django import forms
from basicapp.models import Formulario_Model

# class Formulario(forms.Form): #A classe eh direta
#     name = forms.CharField()
#     email = forms.EmailField()

class Formulario(forms.ModelForm):
    class Meta():
        model = Formulario_Model
        fields = '__all__'
