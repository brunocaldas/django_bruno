from django.shortcuts import render
# from basicapp import forms
from basicapp.forms import Formulario

# Create your views here.

def index(request):
    return render(request, 'basicapp/index.html', context=None)

def form(request):
    Formu0 = Formulario()

    if request.method == 'POST':
        print("The submit button has been pressed")
        Formu = Formulario(request.POST)
        if Formu.is_valid():
            Formu.save(commit=True)
            print('The name was: {}' .format( Formu.cleaned_data['FName'] ) )
            return index(request)

    return render(request, 'basicapp/form.html', context={'form_dict':Formu0})
