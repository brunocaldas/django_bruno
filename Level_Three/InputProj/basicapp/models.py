from django.db import models

# Create your models here.
class Formulario_Model(models.Model):
    FName = models.CharField(max_length=30)
    LName = models.CharField(max_length=30)
    Email = models.EmailField(blank=True, unique=True)
    def __str__(self):
        return self.FName + self.LName
