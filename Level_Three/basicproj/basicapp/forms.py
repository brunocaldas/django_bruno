from django import forms
from django.core import validators

def check_for_z(value): #O que tiver com value Django ja sabe
    if value[0].lower() != 'z':
        raise forms.ValidationError("Must start with z")

class Formulario(forms.Form):
    name = forms.CharField(validators=[check_for_z])
    email = forms.EmailField()
    vemail = forms.EmailField(label="Repeat E-mail: ")
    test = forms.CharField(widget=forms.Textarea)
    # botcatcher = forms.CharField(required=False, widget=forms.HiddenInput)
    botcatcher = forms.CharField(required=False,
                                widget=forms.HiddenInput,
                                validators=[validators.MaxLengthValidator(0)])

    # def clean_botcatcher(self): #O que comeca com clean, O DJanjo ja sabe
    #     botcatcher = self.cleaned_data['botcatcher']
    #     if len(botcatcher) > 0:
    #         raise forms.ValidationError("GOTCHA BOT!")
    #     return botcatcher

    def clean(self): #aqui ele pega todo os dados de clean
        all_cleaned_data = super().clean()
        email = all_cleaned_data['email']
        vmail = all_cleaned_data['vemail']
        if email != vmail:
            raise forms.ValidationError("Emails not match!")
