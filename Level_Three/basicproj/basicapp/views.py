from django.shortcuts import render
from basicapp import forms
# Create your views here.

def index(request):
    return render(request, 'basicapp/index.html', context=None)

def form(request):
    form = forms.Formulario()
    if request.method == 'POST':
        form = forms.Formulario(request.POST)
        if form.is_valid():
            # This is will print in the Terminal
            print('Validation is Perfect')
            print('Name {}' .format(form.cleaned_data['name']) )
            print('E-mail: {}' .format(form.cleaned_data['email']) )
            print('Text: {}' .format(form.cleaned_data['test']) )
    return render(request, 'basicapp/forms.html', context={'formu':form})
