from django.urls import path
from basicapp import views

app_name = 'basicapp'

urlpatterns = [
    path('', views.index, name='index'),
    path('other/', views.other, name='other'),
]
