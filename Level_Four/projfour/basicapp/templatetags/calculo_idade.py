from django import template

register = template.Library()

@register.filter(name='calcula_a_idade')
def idade(ano, string='notok'):
    import datetime
    now = datetime.datetime.now()
    if string=='notok':
        return 'nothing to show'
    return now.year - ano

# register.filter('calcula_a_idade', idade)
