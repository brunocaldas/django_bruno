from django.shortcuts import render

# Create your views here.
def index(request):
    text = 'this text was all written in small caps, but you are seeing this as title'
    ano = 1984
    return render(request, 'basicapp/index.html', context={'texto':text, 'birth':ano})

def other(request):
    return render(request, 'basicapp/other.html')
