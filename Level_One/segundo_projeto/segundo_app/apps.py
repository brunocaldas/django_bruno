from django.apps import AppConfig


class SegundoAppConfig(AppConfig):
    name = 'segundo_app'
