import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'quarto_projeto.settings')

import django
django.setup()

from app4.models import ExerciseUsers

from faker import Faker
fakegen = Faker()

def populate(N=10):
    for entry in range(N):
        fake_fname = fakegen.first_name()
        fake_lname = fakegen.last_name()
        fake_email = fakegen.email()
        webpg = ExerciseUsers.objects.get_or_create(FName=fake_fname,LName=fake_lname,Email=fake_email)[0]

if __name__ == '__main__':
    print('populating script!')
    populate(20)
    print('Populating Complete!')
