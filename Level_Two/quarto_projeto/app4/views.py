from django.shortcuts import render
from app4.models import ExerciseUsers

# Create your views here.

def index(request):
    return render(request, 'index.html', context=None)

def users(request):
    Lista_Usuarios = ExerciseUsers.objects.all()
    Ex_Users = {'access_record':Lista_Usuarios}

    return render(request, 'users.html', context=Ex_Users)
