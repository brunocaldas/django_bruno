from django.apps import AppConfig


class PriAppConfig(AppConfig):
    name = 'pri_app'
