from django.contrib import admin
from pri_app.models import Topic, Webpage, AccessRecord

# Register your models here.
admin.site.register(Webpage)
admin.site.register(Topic)
admin.site.register(AccessRecord)
