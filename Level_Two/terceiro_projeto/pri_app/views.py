from django.shortcuts import render
from pri_app.models import AccessRecord, Webpage, Topic
# Create your views here.

def index(request):
    webpage_list = AccessRecord.objects.order_by('date')
    date_dict = {'access_records':webpage_list}

    #my_dict = {'insert_content': 'Hello I am from pri_app'}
    return render(request, 'pri_app/index.html', context=date_dict)
